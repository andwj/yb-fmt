// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"

func FormatDefinition(t *Token) {
	if t.Kind < TOK_Expr {
		FormatBareElement(t)
		return
	}

	ComputeIndentation(0, t)

	DetermineLineBreaks(t)

	GenerateOutput(t)
}

func FormatBareElement(t *Token) {
	s := t.Str
	if t.Comment != "" {
		s = s + " " + t.Comment
	}
	OutLine("%s", s)
}

func ComputeIndentation(lev int, t *Token) {
	// this computes the indent level of every element, on the
	// (usually false) assumption that each element will be placed
	// on a line by itself.

	if t.Kind < TOK_Expr {
		t.Level = lev
		return
	}

	for idx, sub := range t.Children {
		opener := (idx == 0)
		closer := (idx == len(t.Children)-1)

		new_lev := lev

		if !(opener || closer) {
			new_lev += 1
		}

		ComputeIndentation(new_lev, sub)
	}

	// TODO
	// things like "(" "fun", when separated by comments, should
	// keep "fun" at same indent level as the opening bracket.
}

//----------------------------------------------------------------------

func DetermineLineBreaks(t *Token) {
	for _, sub := range t.Children {
		if sub.Kind >= TOK_Expr {
			DetermineLineBreaks(sub)
		}
	}

	switch t.Kind {
	case TOK_Expr:
		LineBreakExpr(t, 0)

	case TOK_Array:
		LineBreakArray(t, 0)

	case TOK_Map:
		LineBreakTuple(t, 0)

	default:
		// nothing to do
	}
}

func LineBreakArray(t *Token, col int) {
	use_w := UsableWidth(t.Children[0], col)
	ok, w := FitsOnSingleLine(t)

	if ok && w <= use_w {
		return
	}

	for _, sub := range t.Children {
		BreakAfter(sub)
	}
}

func LineBreakTuple(t *Token, col int) {
	use_w := UsableWidth(t.Children[0], col)
	ok, w := FitsOnSingleLine(t)

	if ok && w <= use_w {
		return
	}

	for i := 1; i < len(t.Children)-1; i++ {
		sub := t.Children[i]
		BreakAfter(sub)
	}
}

func LineBreakExpr(t *Token, col int) {
	if len(t.Children) <= 2 {
		return
	}

	// FIXME : keyword could be preceded by TOK_Comment(s)
	head := t.Children[1]

	switch {
	case head.Match("fun"):
		LineBreakFuncDef(t, "fun")
		return

	case head.Match("method"):
		LineBreakFuncDef(t, "method")
		return

	case head.Match("lam"):
		LineBreakFuncDef(t, "lam")
		return

	case head.Match("var"):
		LineBreakVar(t)
		return

	case head.Match("begin"):
		LineBreakBegin(t)
		return

	case head.Match("if"):
		LineBreakIf(t)
		return

	case head.Match("while"):
		LineBreakLoop(t, "while")
		return

	case head.Match("for"):
		LineBreakLoop(t, "for")
		return

	case head.Match("for-each"), head.Match("str-for-each"):
		LineBreakLoop(t, "for")
		return
	}

	if IsMathExpr(t) {
		LineBreakMathExpr(t)
		return
	}

	// everything else is assumed to be a function call
	LineBreakCall(t)
}

func LineBreakCall(t *Token) {
	col := 0

	use_w := UsableWidth(t.Children[0], col)
	ok, w := FitsOnSingleLine(t)

	if ok && w <= use_w {
		return
	}

	LineBreakBegin(t)
}

func LineBreakMathExpr(t *Token) {
	// TODO math exprs
}

func LineBreakVar(t *Token) {
	BreakGroup(t)
}

func LineBreakBegin(t *Token) {
	for i := 1; i < len(t.Children); i++ {
		sub := t.Children[i]
		BreakAfter(sub)
	}

	BreakGroup(t)
}

func LineBreakFuncDef(t *Token, kind string) {
	// FIXME: following does not handle TOK_Comment(s)

	children := t.Children[:]

	if kind == "fun" {
		if len(children) < 5 {
			return
		}
		// skip: ( fun name
		children = children[3:]

	} else if kind == "method" {
		if len(children) < 6 {
			return
		}
		// skip: ( method type name
		children = children[4:]
	} else {
		if len(children) < 4 {
			return
		}
		// skip: ( lam
		children = children[2:]
	}

	params := children[0:]
	result := children[0:0]

	if len(children) > 0 {
		children = children[1:]
	}

	if len(children) > 3 && children[0].Match("->") {
		result = children[0:2]
		children = children[2:]
	}

	body := children

	_ = params
	_ = result

	// top-level definitions are never squeezed onto a single line,
	// but anonymous lambda functions can be....
	if kind == "lam" {
		col := 0

		use_w := UsableWidth(t.Children[0], col)
		ok, w := FitsOnSingleLine(t)

		// prevent when body has 2+ statements
		if len(body) >= 3 {
			ok = false
		}

		if ok && w <= use_w {
			return
		}
	}

	BreakGroup(t)

	// TODO : handle it when params and/or result do not fit on a line

	for _, sub := range body {
		BreakBefore(sub)
		BreakAfter(sub)
	}
}

func LineBreakIf(t *Token) {
	col := 0

	if len(t.Children) <= 7 {
		use_w := UsableWidth(t.Children[0], col)
		ok, w := FitsOnSingleLine(t)

		if ok && w <= use_w {
			return
		}
	}

	// divide the tokens into clauses, where each clause
	// begins with an "if", "elif" or "else" keyword.

	clauses := make([][]*Token, 0)
	clauses = append(clauses, t.Children[1:2])

	cur_start := 1

	for i := 2; i < len(t.Children)-1; i++ {
		tok := t.Children[i]

		if tok.Match("elif") || tok.Match("else") {
			// start a new clause
			cur_start = i
			clauses = append(clauses, t.Children[i:i+1])

		} else {
			// extend the current clause
			L := len(clauses) - 1
			clauses[L] = t.Children[cur_start : i+1]
		}
	}

	// TODO : logic to see if we need a break after each condition
	cond_break := true

	for _, grp := range clauses {
		start := 2
		if cond_break {
			start = 1
		}

		if grp[0].Str == "else" {
			start--
		}

		for i := start; i < len(grp); i++ {
			BreakAfter(grp[i])
		}

		// align "else" and "elif" with the "if"
		if grp[0].Level > 0 {
			grp[0].Level--
		}
	}

	// separate whole expression
	BreakGroup(t)
}

func LineBreakLoop(t *Token, kind string) {
	// TODO for loop with long start and/or end elements
	// TODO for-each loops with long array element

	start := 2
	if kind != "while" {
		start = 4
	}

	for i := start; i < len(t.Children)-1; i++ {
		sub := t.Children[i]
		BreakAfter(sub)
	}

	BreakGroup(t)
}

func BreakGroup(t *Token) {
	BreakBefore(t.Children[0])
	BreakAfter(t.Children[len(t.Children)-1])
}

func BreakAfter(t *Token) {
	if t.Kind >= TOK_Expr {
		t = t.Children[len(t.Children)-1]
	}
	t.After = true
}

func BreakBefore(t *Token) {
	if t.Kind >= TOK_Expr {
		t = t.Children[0]
	}
	t.Before = true
}

func UsableWidth(t *Token, col int) int {
	w := Config.columns - col

	if w < Config.min_w {
		w = Config.min_w
	}

	return w
}

func FitsOnSingleLine(t *Token) (bool, int) {
	if t.Kind < TOK_Expr {
		return true, len(t.Str)
	}

	width := 0

	for idx, sub := range t.Children {
		if sub.Before || sub.After {
			return false, 0
		}

		ok, w := FitsOnSingleLine(sub)
		if !ok {
			return false, 0
		}

		width += w

		if idx > 0 {
			width += 1 // separator space
		}
	}

	return true, width
}

func Depth(t *Token) int {
	depth := 0

	if t.Kind >= TOK_Expr {
		for _, sub := range t.Children {
			d := Depth(sub) + 1
			if d > depth {
				depth = d
			}
		}
	}

	return depth
}

func IsMathExpr(t *Token) bool {
	for i := 1; i < len(t.Children)-1; i++ {
		sub := t.Children[i]
		is_op, _ := IsMathOperator(sub.Str)
		if is_op {
			return true
		}
	}
	return false
}

func IsMathOperator(s string) (bool, int) {
	switch s {
	case "!":
		return true, 0
	case "~":
		return true, 0
	case "||":
		return true, 1
	case "&&":
		return true, 2
	case "==":
		return true, 3
	case "!=":
		return true, 3
	case "<=":
		return true, 4
	case ">=":
		return true, 4
	case "<":
		return true, 4
	case ">":
		return true, 4
	case "+":
		return true, 5
	case "-":
		return true, 5
	case "*":
		return true, 6
	case "/":
		return true, 6
	case "%":
		return true, 6
	case "**":
		return true, 7
	case "|":
		return true, 5
	case "^":
		return true, 5
	case "&":
		return true, 5
	case "<<":
		return true, 6
	case ">>":
		return true, 6
	}

	return false, -1
}

//----------------------------------------------------------------------

type GenInfo struct {
	line  strings.Builder
	level int
}

func GenerateOutput(top *Token) {
	var info GenInfo

	// visit all tokens in first-to-last order
	GenerateRecurse(top, &info)

	if info.line.Len() > 0 {
		GenerateLine(&info)
	}
}

func GenerateRecurse(t *Token, info *GenInfo) {
	if t.Kind >= TOK_Expr {
		for idx, sub := range t.Children {
			// separator (FIXME: be smarter)
			if idx > 1 && idx+1 < len(t.Children) && info.line.Len() > 0 {
				info.line.WriteString(" ")
			}
			GenerateRecurse(sub, info)
		}
		return
	}

	if t.Before && info.line.Len() > 0 {
		GenerateLine(info)
	}

	if info.line.Len() == 0 {
		info.level = t.Level
	}

	info.line.WriteString(t.Str)

	if t.Comment != "" {
		info.line.WriteString(" ")
		info.line.WriteString(t.Comment)
	}

	if t.After {
		GenerateLine(info)
	}
}

func GenerateLine(info *GenInfo) {
	s := IndentString(info.level) + info.line.String()
	OutLine("%s", s)
	info.line.Reset()
}

func IndentString(level int) string {
	if Config.tabs == 0 {
		return fmt.Sprintf("%*s", level*Config.spaces, "")
	}

	if level <= 12 {
		return "\t\t\t\t\t\t\t\t\t\t\t\t"[12-level:]
	}

	res := ""

	for i := 0; i < level; i++ {
		res += "\t"
	}

	return res
}
