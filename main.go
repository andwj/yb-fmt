// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "fmt"

import "gitlab.com/andwj/argv"

var Ok error = nil

var Config struct {
	tabs    int // if > 0, use TABs for indentation
	spaces  int // number of spaces for indentation
	columns int // soft limit of output line width
	min_w   int // minimum width to use
}

const VERSION = "0.1.5"
const DATE_STR = "Feb 2019"

func main() {
	// default configuration
	Config.tabs = 0
	Config.spaces = 3
	Config.columns = 78
	Config.min_w = 36

	var help bool
	var version bool

	// parse the command-line
	argv.Integer("s", "spaces", &Config.spaces, "num", "how many spaces to indent with")
	argv.Integer("t", "tabs", &Config.tabs, "num", "if > 0, use TABs for indentation")
	argv.Integer("c", "columns", &Config.columns, "num", "soft limit on output line width")
	// TODO -W to write file
	// TODO -d to show a diff
	argv.Gap()
	argv.Enabler("h", "help", &help, "display this help text")
	argv.Enabler("v", "version", &version, "display the version")

	err := argv.Parse()
	if err != nil {
		fmt.Fprintf(os.Stderr, "yb-fmt: %s\n", err.Error())
		os.Exit(1)
	}

	names := argv.Unparsed()

	if version {
		ShowVersion()
		os.Exit(0)
	}
	if help || len(names) < 1 {
		ShowHelp()
		os.Exit(0)
	}

	status := ProcessFile(names[0])

	os.Exit(status)
}

func ShowHelp() {
	fmt.Println("Usage: yb-fmt [OPTIONS...] file.yb")
	fmt.Println("")
	fmt.Println("Available options:")

	argv.Display(os.Stdout)
}

func ShowVersion() {
	fmt.Printf("yb-fmt %s  (%s)\n", VERSION, DATE_STR)
}

func ProcessFile(filename string) (status int) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		return 1
	}
	defer f.Close()

	lex := NewLexer(f)

	/// lex.DumpRawTokens()
	/// return 0

	for {
		t := lex.Parse()

		if t.Kind == TOK_EOF {
			break
		}

		if t.Kind == TOK_ERROR {
			fmt.Fprintf(os.Stderr, "\nERROR on line %d: %s\n", t.LineNum, t.Str)
			return 1
		}

		if t.Kind == TOK_Comment {
			OutLine("%s", t.Str)
			continue
		}

		FormatDefinition(t)
	}

	// zero status is Ok
	return 0
}

func OutLine(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Printf("\n")
}
